﻿using Assets.Scripts;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LoadListDungeons : MonoBehaviour
{
    public GameObject cardPrefab;
    public GameObject panel;
    List<Dungeon> dungeons;
    int pageNumber;
    int pagesCount;
    int level;
    int cardsOnPage = 4;
    List<GameObject> cards = new();

    private async void Awake()
    {
        float width = panel.GetComponent<RectTransform>().rect.width;
        float height = panel.GetComponent<RectTransform>().rect.height;
        double cardWidth = height * 0.77;

        cardsOnPage = (int)(width / cardWidth);

        StateService.CompleteChapter(1);
        level = StateService.GetCurrentChapter();

        QueryDB queryDB = new();
        dungeons = await queryDB.ListOfDungeons();
        pageNumber = 1;
        pagesCount = (dungeons.Count - 1) / cardsOnPage + 1;
        
        for(int i = 0; i < cardsOnPage; i++)
        {
            var card = Instantiate(cardPrefab);
            card.transform.SetParent(panel.transform);
            card.SetActive(false);
            card.transform.localScale = Vector3.one;
            cards.Add(card);
        }

        UpdateCards();
    }

    void UpdateCards()
    {
        int start = (pageNumber - 1) * cardsOnPage;
        int end = pageNumber * cardsOnPage - 1;

        if (start >= dungeons.Count)
        {
            pageNumber--;
            return;
        }

        end = end > dungeons.Count - 1 ? dungeons.Count - 1: end;
        int count = end - start + 1;

        int j = 0;

        for(int i = start; j < count; i++, j++)
        {
            var card = cards[j];
            card.transform.Find("Theme").GetComponent<TMP_Text>().text = dungeons[i].Topic;
            card.transform.Find("tasksCount").GetComponent<TMP_Text>().text = "Количество заданий:" + dungeons[i].CountTasks.ToString();
            card.transform.Find("coinsCount").GetComponent<TMP_Text>().text = "0/" + dungeons[i].CoinsSum.ToString();
            card.GetComponent<GoToDungeon>().name = dungeons[i].Name;
            card.SetActive(true);

            LockCard(card, i + 1 >= level);
        }

        for (; j < cards.Count; j++)
        {
            cards[j].SetActive(false);
        }
    }

    private static void LockCard(GameObject card, bool isLock)
    {
        card.transform.Find("lockPanel").gameObject.SetActive(isLock);
        card.GetComponent<Button>().enabled = !isLock;
    }

    public void NextPage()
    {
        pageNumber++;
        UpdateCards();
    }

    public void PrevPage()
    {
        if (pageNumber < 2)
            return;

        pageNumber--;
        UpdateCards();
    }
}
