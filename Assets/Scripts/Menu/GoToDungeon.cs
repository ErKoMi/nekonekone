using UnityEngine;
using UnityEngine.SceneManagement;

public class GoToDungeon : MonoBehaviour
{
    public string name;

    public void Excecute()
    {
        ExitFromDungeon.ExitPlace = ExitPlace.Menu;
        SceneManager.LoadScene(name);
    }
}
