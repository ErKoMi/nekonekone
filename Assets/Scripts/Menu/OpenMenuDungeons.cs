using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OpenMenuDungeons : MonoBehaviour
{
    public void Open()
    {
        SceneManager.LoadSceneAsync("DungeonMenu");
    }

    public void Close()
    {
        SceneManager.LoadSceneAsync("Menu");
    }
}
