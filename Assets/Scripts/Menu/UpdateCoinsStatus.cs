using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UpdateCoinsStatus : MonoBehaviour
{
    public TMP_Text coinsText;

    private void Awake()
    {
        int balance = StateService.GetWalletBalance();
        coinsText.text = balance.ToString();

        StateService.OnbalanceChanged += StateService_OnbalanceChanged;
    }

    private void StateService_OnbalanceChanged(int balance)
    {
        coinsText.text = balance.ToString();
    }
}
