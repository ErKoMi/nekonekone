using Assets.Scripts;
using Assets.Scripts.Models;
using Assets.Scripts.TaskSystem;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

public class QueryDB : MonoBehaviour {

    void Start()
    {
        DataTable scoretable = MyDataBase.GetTable("SELECT * FROM Item");
        GetComponent<TMP_Text>().text = scoretable.Rows[0][1].ToString();
    }

    public async Task<List<string>> ListOfItems() => await Task.Run(() => GetTitles("Item"));

    public async Task<List<string>> ListOfTopic() => await Task.Run(() => GetTitles("Topic"));

    public async Task<List<string>> ListOfSubTopic() => await Task.Run(() => GetTitles("SubTopic"));

    private List<string> GetTitles(string tableName)
    {
        return MyDataBase.GetTable($"SELECT title FROM {tableName}").Rows.Cast<DataRow>().Select(row => (string)row["title"]).ToList();
    }

    public async Task<List<TaskBase>> GiveTaskSubTopic(string nameSubtopic) => await Task.Run(() =>
    {
        DataTable subTaskTable = MyDataBase.GetTable($"SELECT task.id, task.description, subtopic.title, task.cost, rightanswer.answer FROM Task task, SubTopic subtopic, RightAnswer rightanswer WHERE task.subtopic_id = subtopic.number and task.rightanswer_id = rightanswer.id and subtopic.title = \"{nameSubtopic}\"");

        return GetTaskList(subTaskTable);
    });

    public async Task<List<TaskBase>> GiveTaskTopic(string nameTopic) => await Task.Run(() => {
        DataTable taskTable = MyDataBase.GetTable($"SELECT task.id, task.description, subtopic.title, task.cost, rightanswer.answer FROM SubTopic subtopic, Topic topic, Task task, RightAnswer rightanswer WHERE topic.title = \"{nameTopic}\" and subtopic.topic_id = topic.number and task.subtopic_id = subtopic.number and task.id = rightanswer.id");

        return GetTaskList(taskTable);
    });

    public async Task<List<TaskBase>> GiveTaskItem(string nameItem) => await Task.Run(() => {
        DataTable taskTable = MyDataBase.GetTable($"SELECT DISTINCT task.id, task.description, task.cost, subtopic.title, rightanswer.answer FROM Task task, Item item, Topic topic, SubTopic subtopic, RightAnswer rightanswer WHERE item.title = \"{nameItem}\" and item.id = topic.item_id and topic.number = subtopic.topic_id and subtopic.topic_id = task.subtopic_id and task.rightanswer_id = rightanswer.id");

        return GetTaskList(taskTable);
    });

    public async Task<List<Dungeon>> ListOfDungeons() => await Task.Run(async () =>
    {
        DataTable table = MyDataBase.GetTable($"SELECT location.title as name, topic.title as topic FROM Location location, Topic topic WHERE location.topic_id = topic.number");

        List<Dungeon> list = new List<Dungeon>();

        foreach (DataRow row in table.Rows)
        {
            Dungeon dungeon = new()
            {
                Name = (string)row["name"],
                Topic = (string)row["topic"],
            };

            List<long> res = await CountOfTaskByTopic(dungeon.Topic);
            dungeon.CountTasks = (int)res[0];
            dungeon.CoinsSum = (int)res[1];
            list.Add(dungeon);
        }

        return list;
    });

    public async Task<List<long>> CountOfTaskByTopic(string topic) => await Task.Run(() =>
    {
        DataTable table = MyDataBase.GetTable($"SELECT COUNT(task.id), SUM(task.cost) FROM SubTopic subtopic, Topic topic, Task task WHERE topic.title = \"{topic}\" and subtopic.topic_id = topic.number and task.subtopic_id = subtopic.number");

        return new List<long>() { (long)table.Rows[0][0], (long)table.Rows[0][1] };
    });


    List<TaskBase> GetTaskList(DataTable table) {

        List<TaskBase> taskItems = new();
        foreach (DataRow row in table.Rows)
        {
            DataTable falseAnswers = MyDataBase.GetTable($"SELECT falseanswer.answer FROM FalseAnswer falseanswer WHERE FalseAnswer.task_id = {row["id"]}");
            var taskFalseAnswers = falseAnswers.Rows.Cast<DataRow>().Select(row => (string)row["answer"]).ToList();
            var allAnswers = new List<string>() { (string)row["answer"] };
            allAnswers.AddRange(taskFalseAnswers);
            if (taskFalseAnswers.Count != 0)
            {
                TaskChoose taskChoose = new()
                {
                    taskDescription = (string)row["description"],
                    subtopic = (string)row["title"],
                    correctAnswer = 0,
                    answers = allAnswers,
                    coinCost = Convert.ToUInt32((long)row["cost"])
                };
                taskItems.Add(taskChoose);
            }
            else
            {
                TaskInput taskInput = new()
                {
                    taskDescription = (string)row["description"],
                    subtopic = (string)row["title"],
                    correctAnswers = new List<string>() { (string)row["answer"] },
                    coinCost = Convert.ToUInt32((long)row["cost"])
                };
                taskItems.Add(taskInput);
            }
        }
        return taskItems;
    }

    public async Task<List<HelpInfo>> GetHelpInfoByLocation(string location, string item) => await Task.Run(() =>
    {
        DataTable table = MyDataBase.GetTable($"SELECT help.title, help.text FROM HelpInfo help, Location location, Item item WHERE item.title = \"{item}\" AND location.title = \"{location}\" AND item.id = help.item AND location.id = help.location");

        return table.Rows.Cast<DataRow>().Select(row => new HelpInfo { Title = (string)row["title"], Text = (string)row["text"] }).ToList();
    });
}
