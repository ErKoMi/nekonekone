using UnityEngine;

public class SoundRun : MonoBehaviour
{
    public AudioSource moveSound;
    public Joystick joystick;

    void Update()
    {
        if (Mathf.Abs(joystick.Horizontal) > 0.35f && Mathf.Abs(joystick.Vertical) > 0.35f)
        {
            if (moveSound.isPlaying) return;
            moveSound.Play();
        }
        else
        {
            moveSound.Stop();
        }
    }
}