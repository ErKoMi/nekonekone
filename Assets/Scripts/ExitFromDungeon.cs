using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitFromDungeon : MonoBehaviour
{
    public static ExitPlace ExitPlace { get; set; } = ExitPlace.Menu;

    public void Exit()
    {
        switch (ExitPlace)
        {
            case ExitPlace.Novel:
                SwitchToNovel exit = new();
                exit.Excecute();
                break;
            case ExitPlace.Menu:
                SceneManager.LoadSceneAsync("DungeonMenu");
                break;
            default:
                break;
        }
    }   
}

public enum ExitPlace { Novel, Menu }
