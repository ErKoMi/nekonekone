﻿using Naninovel;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Novel
{
    [CommandAlias("dungeon")]
    public class SwitchToDungeon : Command
    {
        public StringParameter name;

        public override async UniTask ExecuteAsync(AsyncToken asyncToken = default)
        {
            var inputManager = Engine.GetService<IInputManager>();
            inputManager.ProcessInput = false;

            // 2. Stop script player.
            var scriptPlayer = Engine.GetService<IScriptPlayer>();
            scriptPlayer.Stop();

            Engine.GetService<IStateManager>().SaveGameAsync("dungeon");

            // 4. Load scene
            await SceneManager.LoadSceneAsync(name.Value);

            // 5. Switch cameras.
            var advCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
            advCamera.enabled = true;
            var naniCamera = Engine.GetService<ICameraManager>().Camera;
            naniCamera.enabled = false;

            Engine.FindObject("InputManager")?.SetActive(false);

            ExitFromDungeon.ExitPlace = ExitPlace.Novel;
            StateService.SetWalletBalance(int.Parse(Engine.GetService<ICustomVariableManager>().GetVariableValue("coins")));
        }
    }
}
