using Naninovel;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class HomeScript : MonoBehaviour
{
    public async void GoHomeScreenAsync()
    {
        var state = Engine.GetService<IStateManager>();
        await state.SaveGameAsync("main");

        // 4. Load scene
        await SceneManager.LoadSceneAsync("menu");

        // 5. Switch cameras.
        var naniCamera = Engine.GetService<ICameraManager>().Camera;
        naniCamera.enabled = false;
        var advCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
        advCamera.enabled = true;

        Engine.Reset();

        var inputManager = Engine.GetService<IInputManager>();
        inputManager.ProcessInput = false;

        Engine.FindObject("InputManager").SetActive(false);
        Engine.FindObject("UI").SetActive(false);

        StateService.SetWalletBalance(int.Parse(Engine.GetService<ICustomVariableManager>().GetVariableValue("coins")));
    }
}
