using Naninovel;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class SwitchToNovel : MonoBehaviour
{
    public void Excecute()
    {
        if (!Engine.Initialized)
        {
            Engine.OnInitializationFinished += Engine_OnInitializationFinished;
            RuntimeInitializer.InitializeAsync();
        }
        else
        {
            PlayScriptAsync();
        }
    }

    private void Engine_OnInitializationFinished()
    {
        PlayScriptAsync();
    }

    async Task PlayScriptAsync()
    {
        GameObject.Find("Interface")?.SetActive(false);
        Engine.FindObject("InputManager").SetActive(true);

        EventSystem eventSystem = GameObject.Find("EventSystem").GetComponent<EventSystem>();
        eventSystem.enabled = false;

        SceneManager.LoadSceneAsync("Empty");
        var camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        camera.enabled = false;

        await Engine.GetService<IStateManager>().LoadGameAsync("dungeon");

        var player = Engine.GetService<IScriptPlayer>();
        player.Play(player.Playlist, player.PlayedIndex + 1);

        Engine.GetService<ICameraManager>().Camera.enabled = true;

        Engine.GetService<IInputManager>().ProcessInput = true;
        await Engine.GetService<IStateManager>().SaveGameAsync("main");

        Engine.GetService<ICustomVariableManager>().SetVariableValue("coins", StateService.GetWalletBalance().ToString());

    }
}
