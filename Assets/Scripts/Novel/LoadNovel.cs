using Naninovel;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class LoadNovel : MonoBehaviour
{
    public string startScript = "FirstChapter"; 

    public void Load()
    {
        if (!Engine.Initialized)
        {
            Engine.OnInitializationFinished += Engine_OnInitializationFinished;
            RuntimeInitializer.InitializeAsync();
        }
        else
        {
            PlayScriptAsync();
        }
    }

    private void Engine_OnInitializationFinished()
    {
        PlayScriptAsync();
    }

    private async Task PlayScriptAsync()
    {
        EventSystem eventSystem = GameObject.Find("EventSystem").GetComponent<EventSystem>();
        eventSystem.enabled = false;
        Engine.FindObject("InputManager")?.SetActive(true);
        Engine.FindObject("UI")?.SetActive(true);

        SceneManager.LoadSceneAsync("Empty");
        var camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        camera.enabled = false;

        var stateManager = Engine.GetService<IStateManager>();

        if (stateManager.GameSlotManager.SaveSlotExists("main"))
        {
            await stateManager.LoadGameAsync("main");
        }
        else
        {
            await Engine.GetService<IScriptPlayer>().PreloadAndPlayAsync(startScript);
        }


        GameObject.FindObjectOfType<EventSystem>().enabled = true;

        Engine.GetService<ICameraManager>().Camera.enabled = true;

        Engine.GetService<IInputManager>().ProcessInput = true;

        Engine.GetService<ICustomVariableManager>().OnVariableUpdated += SwitchToNovel_OnVariableUpdated;
    }

    private void SwitchToNovel_OnVariableUpdated(CustomVariableUpdatedArgs obj)
    {
        if (obj.Name == "coins")
        {
            StateService.SetWalletBalance(int.Parse(obj.Value));
        }
    }
}
