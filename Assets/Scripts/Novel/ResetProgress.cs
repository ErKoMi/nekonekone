using Naninovel;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetProgress : MonoBehaviour
{
    public void Excecute()
    {
        if (!Engine.Initialized)
        {
            Engine.OnInitializationFinished += Engine_OnInitializationFinished;
            RuntimeInitializer.InitializeAsync();
        }
        else
        {
            Res();
        }
    }

    private void Engine_OnInitializationFinished()
    {
        Res();
    }

    private void Res()
    {
        var stateManager = Engine.GetService<IStateManager>();
        stateManager.GameSlotManager.DeleteSaveSlot("main");
        Engine.Reset();
    }
}
