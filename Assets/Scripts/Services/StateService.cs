using System;
using UnityEngine;

public static class StateService
{
    public static event Action<int> OnbalanceChanged;
    const string CHAPTER = "Chapter";
    const string BALANCE = "Balance";

    public static void CompleteChapter(int chapter)
    {
        PlayerPrefs.SetInt(CHAPTER, chapter + 1);
    }

    public static int GetCurrentChapter()
    {
        return PlayerPrefs.GetInt(CHAPTER, 1);
    }

    public static int GetWalletBalance()
    {
        if (PlayerPrefs.HasKey(BALANCE))
        {
            return PlayerPrefs.GetInt(BALANCE);
        }

        PlayerPrefs.SetInt(BALANCE, 0);
        return 0;
    }

    public static void SetWalletBalance(int balance)
    {
        PlayerPrefs.SetInt(BALANCE, balance);
        OnbalanceChanged?.Invoke(balance);
    }

    public static void ResetSaves()
    {
        PlayerPrefs.DeleteAll();
    }
}

