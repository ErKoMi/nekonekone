﻿using UnityEngine;
using System.Data;
using Mono.Data.Sqlite;
using System.IO;

static class MyDataBase
{
    private const string DBName = "DataBase";
    private static SqliteConnection connection;
    private static SqliteCommand command;

    static MyDataBase()
    {
        string con = SetDataBaseClass.SetDataBase(DBName + ".db");
        connection = new SqliteConnection(con);
        command = connection.CreateCommand();
    }

    /// <summary> Ýòîò ìåòîä îòêðûâàåò ïîäêëþ÷åíèå ê ÁÄ. </summary>
    private static void OpenConnection()
    {
        connection.Open();
    }

    /// <summary> Ýòîò ìåòîä çàêðûâàåò ïîäêëþ÷åíèå ê ÁÄ. </summary>
    public static void CloseConnection()
    {
        connection.Close();
        command.Dispose();
    }

    /// <summary> Ýòîò ìåòîä âûïîëíÿåò çàïðîñ query. </summary>
    /// <param name="query"> Ñîáñòâåííî çàïðîñ. </param>
    public static void ExecuteQueryWithoutAnswer(string query)
    {
        OpenConnection();
        command.CommandText = query;
        command.ExecuteNonQuery();
        CloseConnection();
    }

    /// <summary> Ýòîò ìåòîä âûïîëíÿåò çàïðîñ query è âîçâðàùàåò îòâåò çàïðîñà. </summary>
    /// <param name="query"> Ñîáñòâåííî çàïðîñ. </param>
    /// <returns> Âîçâðàùàåò çíà÷åíèå 1 ñòðîêè 1 ñòîëáöà, åñëè îíî èìååòñÿ. </returns>
    public static string ExecuteQueryWithAnswer(string query)
    {
        OpenConnection();
        command.CommandText = query;
        var answer = command.ExecuteScalar();
        CloseConnection();

        if (answer != null) return answer.ToString();
        else return null;
    }

    /// <summary> Ýòîò ìåòîä âîçâðàùàåò òàáëèöó, êîòîðàÿ ÿâëÿåòñÿ ðåçóëüòàòîì âûáîðêè çàïðîñà query. </summary>
    /// <param name="query"> Ñîáñòâåííî çàïðîñ. </param>
    public static DataTable GetTable(string query)
    {
        OpenConnection();

        SqliteDataAdapter adapter = new(query, connection);

        DataSet DS = new();
        adapter.Fill(DS);
        adapter.Dispose();

        CloseConnection();

        return DS.Tables[0];
    }
}