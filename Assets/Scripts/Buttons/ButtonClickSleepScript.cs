using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class ButtonClickSleepScript : MonoBehaviour
{
    public AudioClip _clickSound;

    public void PlaySoundOnClick()
    {
        SoundManager.Instance.PlaySound(_clickSound);
        Thread.Sleep(300);
    }
}
