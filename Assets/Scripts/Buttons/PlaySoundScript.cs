using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundScript : MonoBehaviour
{
    public AudioClip _clickSound;

    public void PlaySoundOnClick()
    {
        SoundManager.Instance.PlaySound(_clickSound);
    }
}
