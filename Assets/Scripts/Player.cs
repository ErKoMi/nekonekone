using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Player : MonoBehaviour
{
    public ControlType controlType;
    public Joystick joystick;
    public Animator animator;
    [SerializeField]private const float _speed = 17.0f;  //�������� ���������

    public enum ControlType { Android }

    private Rigidbody2D _rb;
    private Vector2 _moveInput;  //���������� ������� ��������
    private Vector2 _moveVelocity; //�������� �������� ������ � �����-�� �����������
    private bool _facingRight = true; //����� ������� ������, ��������� ��� ��������� ���������

    // Start is called before the first frame update
    void Start()
    {

        _rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        _moveInput = new Vector2(joystick.Horizontal, joystick.Vertical); //��� ������������ � ������� ��������
        if (_moveInput.x != 0) {
            animator.SetBool("isStopping", false);
        } else {
            animator.SetBool("isStopping", true);
        }
        _moveVelocity = _moveInput.normalized * _speed;
        if (!_facingRight && _moveInput.x > 0) {
            FlipHorizontal();
        }
        else if (_facingRight && _moveInput.x < 0)
        {
            FlipHorizontal();
        }
    }

    private void FixedUpdate()
    {
        _rb.MovePosition(_rb.position + _moveVelocity * Time.fixedDeltaTime);
    }

    private void FlipHorizontal()
    {
        _facingRight = !_facingRight;
        Vector3 Scaler = transform.localScale;
        Scaler.x *= -1;
        transform.localScale = Scaler;
    }
}
