﻿namespace Assets.Scripts.TaskSystem
{
    public abstract class TaskBase
    {
        public string subtopic;
        public string taskDescription;
        public uint coinCost;

        public abstract bool IsAnswerCorrect(string answer);
    }
}
