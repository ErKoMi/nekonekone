﻿using System.Collections.Generic;

namespace Assets.Scripts.TaskSystem
{
    public class TaskChoose: TaskBase
    {
        public int correctAnswer;
        public List<string> answers;

        public override bool IsAnswerCorrect(string answer)
        {
            return answers.IndexOf(answer) == correctAnswer;
        }
    }
}
