﻿using Assets.Scripts.TaskSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetrics;

public class TaskManager : MonoBehaviour
{
    public GameObject taskInputBox;
    public GameObject taskChooseBox;

    //Task Base
    public Animator startButton;

    public GameObject messageSreen;
    public GameObject blockSreen;

    public int secondsToWait;
    public int secondsToBlockScreen;

    public TaskStorage taskStorage;

    [HideInInspector]
    public TaskAnimator taskAnimator;

    [HideInInspector]
    public TaskBase task;

    //Task Choose
    public GameObject buttonPrefab;

    public Sprite wrongAnswer;
    public Sprite rightAnswer;

    public void StartTask(string taskSubtopic, TaskAnimator taskAnimator)
    {
        if (taskAnimator.task == null)
        {
            task = GetTaskBySubtopic(taskSubtopic);
        }
        else
        {
            task = taskAnimator.task;
        }
        this.taskAnimator = taskAnimator;
        FillTaskBox();
        startButton.SetBool("interectionButtonShow", false);
    }
    public TaskBase GetTaskBySubtopic(string taskSubtopic)
    {
        return taskStorage.GiveNewTask(taskSubtopic);
    }

    private void FillTaskBox()
    {
        Transform transform;
        switch (task)
        {
            case TaskInput:
                transform = taskInputBox.transform;
                FillTaskBoxComponents(transform);
                SetTaskBoxShow(taskInputBox, true);
                break;
            case TaskChoose:
                transform = taskChooseBox.transform;
                FillTaskBoxComponents(transform);
                AddButtons(transform.Find("PanelForButtons"));
                SetTaskBoxShow(taskChooseBox, true);
                break;
            default:
                break;
        }
    }

    private void FillTaskBoxComponents(Transform transform)
    {
        transform.Find("TaskSubtopic").GetComponentInChildren<TMP_Text>().text = task.subtopic;
        transform.Find("TaskDescription").GetComponentInChildren<TMP_Text>().text = task.taskDescription;
        transform.Find("CoinCost").GetComponentInChildren<TMP_Text>().text = task.coinCost.ToString();
    }

    public void AddButtons(Transform panelToAttachButtonsTo)
    {
        List<string> answersToAdd = AnswersToAdd();
        for (int i = 0; i < (task as TaskChoose).answers.Count; i++)
        {
            string textToAdd = answersToAdd[i];
            GameObject button = Instantiate(buttonPrefab);
            button.transform.SetParent(panelToAttachButtonsTo);
            button.GetComponentInChildren<TMP_Text>().text = textToAdd;
            button.GetComponent<Button>().onClick.AddListener(() => ButtonClick(textToAdd));
        }
    }

    public List<string> AnswersToAdd()
    {
        System.Random rnd = new();
        List<string> answersToAdd = new((task as TaskChoose).answers);
        for (int i = answersToAdd.Count - 1; i > 0; i--)
        {
            int j = rnd.Next(0, i + 1);
            (answersToAdd[j], answersToAdd[i]) = (answersToAdd[i], answersToAdd[j]);
        }
        return answersToAdd;
    }

    public void ButtonClick(string buttonText)
    {
        CheckAnswer(buttonText);
    }

    public void CheckAnswer(string userAnswer)
    {
        if (task.IsAnswerCorrect(userAnswer))
        {
            taskAnimator.isDone = true;
            StartCoroutine(ShowMessageSreen("Молодец!"));
            AddCoins();
            EndTask(true);
        }
        else
        {
            TaskBase newTask = GetTaskBySubtopic(task.subtopic);
            switch (task)
            {
                case TaskInput:
                    StartCoroutine(ShowAnswer(taskInputBox, newTask));
                    break;
                case TaskChoose:
                    StartCoroutine(ShowAnswer(userAnswer, newTask, taskChooseBox));
                    break;
            }
        }
    }

    private void AddCoins()
    {
        StateService.SetWalletBalance(StateService.GetWalletBalance() + (int)task.coinCost);
    }

    public IEnumerator ShowAnswer(GameObject taskBox, TaskBase newTask)
    {
        bool flag = newTask != null;
        messageSreen.GetComponentInChildren<TMP_Text>().text = "Не правильно!";
        messageSreen.SetActive(true);
        yield return new WaitForSeconds(secondsToWait);
        messageSreen.SetActive(false);
        if (flag)
            taskInputBox.transform.Find("InputField").GetComponentInChildren<TMP_InputField>().text =
                "Ответ: " + (task as TaskInput).correctAnswers[0];
        blockSreen.SetActive(true);
        yield return new WaitForSeconds(secondsToBlockScreen);
        blockSreen.SetActive(false);
        taskInputBox.transform.Find("InputField").GetComponentInChildren<TMP_InputField>().text = "";
        if (flag)
        {
            SetTaskBoxShow(taskBox, false);
            UpdateTaskBox(newTask);
        }
    }

    public IEnumerator ShowAnswer(string userAnswer, TaskBase newTask, GameObject taskBox)
    {
        bool flag = newTask != null;
        messageSreen.GetComponentInChildren<TMP_Text>().text = "Не правильно!";
        messageSreen.SetActive(true);
        yield return new WaitForSeconds(secondsToWait);
        messageSreen.SetActive(false);
        foreach (Transform child in taskBox.transform.Find("PanelForButtons"))
        {
            if (child.GetComponentInChildren<TMP_Text>().text == userAnswer)
            {
                child.GetComponent<Button>().image.sprite = wrongAnswer;
            }
            if (child.GetComponentInChildren<TMP_Text>().text == (task as TaskChoose).answers[(task as TaskChoose).correctAnswer] && flag)
            {
                child.GetComponent<Button>().image.sprite = rightAnswer;
            }
        }
        blockSreen.SetActive(true);
        yield return new WaitForSeconds(secondsToBlockScreen);
        blockSreen.SetActive(false);
        if (flag)
        {
            SetTaskBoxShow(taskBox, false);
            UpdateTaskBox(newTask);
        }
    }

    private void UpdateTaskBox(TaskBase newTask)
    {
        switch (task)
        {
            case TaskInput:
                taskInputBox.transform.Find("InputField").GetComponentInChildren<TMP_InputField>().text = "";
                break;
            case TaskChoose:
                DeleteAllButtons(taskChooseBox.transform.Find("PanelForButtons"));
                break;
            default:
                break;
        }
        task = newTask;
        FillTaskBox();
    }

    public void CheckClick()
    {
       CheckAnswer(taskInputBox.transform.Find("InputField").GetComponentInChildren<TMP_InputField>().text);
    }

    private IEnumerator ShowMessageSreen(string showText)
    {
        messageSreen.GetComponentInChildren<TMP_Text>().text = showText;
        messageSreen.SetActive(true);
        yield return new WaitForSeconds(secondsToWait);
        messageSreen.SetActive(false);
    }

    public void EndTask(bool taskIsDone)
    {
        if(!taskIsDone && task != null)
            taskAnimator.task = task;
        switch (task)
        {
            case TaskInput:
                SetTaskBoxShow(taskInputBox, false);
                taskInputBox.transform.Find("InputField").GetComponentInChildren<TMP_InputField>().text = "";
                break;
            case TaskChoose:
                SetTaskBoxShow(taskChooseBox, false);
                DeleteAllButtons(taskChooseBox.transform.Find("PanelForButtons"));
                break;
            default:
                break;
        }
        task = null;
    }

    private void SetTaskBoxShow(GameObject taskBox, bool show)
    {
        taskBox.GetComponent<Animator>().SetBool("taskBoxShow", show);
    }

    public void CloseTask()
    {
        EndTask(false);
        startButton.SetBool("interectionButtonShow", true);
    }

    public void DeleteAllButtons(Transform panelToAttachButtonsTo)
    {
        foreach (Transform child in panelToAttachButtonsTo)
        {
            Destroy(child.gameObject);
        }
    }
}
