using Assets.Scripts.TaskSystem;
using UnityEngine;

public class TaskAnimator : MonoBehaviour
{
    public Animator startAnimation;
    public TaskTrigger trigger; 
    public TaskManager taskManager;

    public string taskSubtopic;
    public bool isDone;

    [HideInInspector]
    public TaskBase task;

    public void OnTriggerEnter2D(Collider2D collision) 
    {
        if (isDone != true)
        {
            trigger.taskSubtopic = taskSubtopic;
            trigger.taskAnimator = this;
            startAnimation.SetBool("interectionButtonShow", true);
        }
    }
    public void OnTriggerExit2D(Collider2D collision)
    {
        startAnimation.SetBool("interectionButtonShow", false);
        taskManager.EndTask(false);
    }
}
