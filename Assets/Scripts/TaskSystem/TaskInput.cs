﻿using System.Collections.Generic;

namespace Assets.Scripts.TaskSystem
{
    public class TaskInput: TaskBase
    {
        public List<string> correctAnswers;

        public override bool IsAnswerCorrect(string answer)
        {
            return correctAnswers.Contains(answer);
        }
    }
}
