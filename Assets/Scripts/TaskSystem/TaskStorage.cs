using Assets.Scripts.TaskSystem;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TaskStorage : MonoBehaviour
{
    public string topic;

    private List<TaskBase> baseTasks;

    void Start()
    {
        FillTasks();
    }

    private async void FillTasks() 
    {
        QueryDB query = new();
        baseTasks = await query.GiveTaskTopic(topic);
    }

    public TaskBase GiveNewTask(string taskSubtopic)
    {
        System.Random rand = new();
        TaskBase task = baseTasks.Where(t => t.subtopic == taskSubtopic)
                                   .OrderBy(t => rand.Next())
                                   .FirstOrDefault();
        baseTasks.Remove(task);
        return task;
    }
}
