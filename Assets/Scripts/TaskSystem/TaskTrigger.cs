using UnityEngine;

public class TaskTrigger : MonoBehaviour
{
    public TaskManager taskManager;

    [HideInInspector]
    public TaskAnimator taskAnimator;

    [HideInInspector]
    public string taskSubtopic;

    public void TriggerDialogue()
    {
        taskManager.StartTask(taskSubtopic, taskAnimator);
    }
}
