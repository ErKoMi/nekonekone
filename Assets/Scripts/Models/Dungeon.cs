﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public class Dungeon
    {
        public string Name { get; set; }
        public string Topic { get; set; }
        public int CountTasks { get; set; }
        public int CoinsSum { get; set; }
    }
}
