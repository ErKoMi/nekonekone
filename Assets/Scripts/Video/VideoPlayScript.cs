using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoPlayScript : MonoBehaviour
{
    public VideoPlayer loadVideoPlayer;

    public void StartPlayVideo()
    {
        loadVideoPlayer.Play();
    }
}
