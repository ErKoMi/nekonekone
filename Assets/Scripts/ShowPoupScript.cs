using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowPoupScript : MonoBehaviour
{
    public GameObject canvas;
    Animator animator;

    // Start is called before the first frame update
    void Start()
    {
       animator = canvas.GetComponent<Animator>(); 
    }

    public void OnShowClick()
    {
        if (animator != null)
        {
            animator.SetBool("IsShow", true);
        }
    }

    public void OnCloseClick()
    {
        if (animator != null)
        {
            animator.SetBool("IsShow", false);
        }
    }
}
