using Assets.Scripts.Models;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

public class HelpManager : MonoBehaviour
{
    public TMP_Text title;
    public TMP_Text text;
    public GameObject panel;

    List<HelpInfo> info;
    public string location;
    public string item;

    int currentIndex = 0;

    async void Start()
    {
        QueryDB query = new QueryDB();
        info = await query.GetHelpInfoByLocation(location, item);
    }

    public void Show()
    {
        panel?.SetActive(true);
        UpdateInfo();
    }

    public void Hide()
    {
        panel?.SetActive(false);
    }

    public void NextPage()
    {
        if (currentIndex >= info.Count - 1)
            return;

        currentIndex++;
        UpdateInfo();
    }

    public void PrevPage()
    {
        if (currentIndex == 0)
            return;
        currentIndex--;
        UpdateInfo();
    }

    void UpdateInfo()
    {
        title.text = info[currentIndex].Title;
        text.text = info[currentIndex].Text;
    }
}
